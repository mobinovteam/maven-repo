#!/bin/bash

if [ -z "$M2" ]; then
	echo " "
	echo " "
	echo "Configure a variável de ambiente do MAVEN"
	echo " "
	echo " "
	exit;
fi


echo "********************"	
echo "groupid artifactId version jarfile"
echo " "
echo "groupid-> ${1}"
echo "artifactId-> ${2}"
echo "version-> ${3}"
echo "jarfile-> ${4}"
echo "********************"


if [ "$1" = "" ]
then
  echo "Digite o groupId"
  exit
fi
if [ "$2" = "" ]
then
  echo "Digite o artifactId"
  exit
fi
if [ "$3" = "" ]
then
  echo "Digite a versão"
  exit
fi
if [ "$4" = "" ]
then
  echo "Digite caminho para o jar file"
  exit
fi

$M2/bin/mvn install:install-file -DgroupId=${1} -DartifactId=${2} -Dversion=${3} -Dfile=${4} -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=repository  -DcreateChecksum=true

echo "Adicionando libs no repositório"

git status
git add *
git commit -a -m "adicionando libs"
git push
